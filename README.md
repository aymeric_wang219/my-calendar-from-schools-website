# DISCLAIMER #

My code is taylorsuited for my college's website and not for other websites.

# Context of this project #

## Intro

This project is more of an exercice that attempts to import my university calendar to my personal calendar. I wanted to come up with such solution since the only way for accessing it is via undergraduate accounts. Although there is a link that allows you to download .ical files on your desktop, this feature has been removed from students' accounts in fear of overwhelming servers.
Naturally, it does anyway and client requests take way too much time to be executed. Notwithstanding this absurdity, I am not going to complain. University IT services are stable and functional despite the reduced budget invested in them.

I did not break into college servers fearing breaking its architecture but also, I confess, facing ultimate retaliations. As I am not an expert in server administration, I only tackled the problem with curl commands equivalents and "click on this button on web page" to get account data.


## How I tried to solve the problem

I used Python because I am familiar with it and I wanted to have a quick solution. It was not easy and I abandonned the project out of spending to much time on it (a driver license matters more to me for some reason). I tried two unsuccessful approaches still.

1. Automation to log into my account (Attempt_1 rep)

At first, this seemed to be the best compromise. What I did was manually login to my account page and access the calendar web page. I did it with without any difficulty.
Once done, I thought I could fetch relevant XML/CSS/HTML contents in web page source code with selenium libraries. To do that, I would stock the source code in a .txt file, fetch incoming events in its content with string analysis and then, write an .ics or .ical file.

However, the web page source code has too many sub-divisions and therefore are not displayed by the `browser.page_source()` function of the selenium library. I did not find any alternative within a reasonable time.

Too bad. Close to something. :/


2. Curling a web page content (Attempt_2 rep)

I realized that a specific link from the university's website downloaded a .ical file with all upcoming events. "Yeepee" I tought, but once again another fail.

I could download the web page content using a curl-like shell command integrated in selenium libraries. Then I could open the .ical file and delete its unrelevant events. Afterwise, I would rewrite the file and I open it manually in my calendar manager.
Nevertheless, the link I found only provides a .ical file containing *the latest modifications* in calendars of all university's members.

So close again, a real shame! It works very well.


3. and 4. are hybrid combinations of attempts 1. and 2. Again, not much result in a reasonable time... not to mention my university now provides such functionality!

## Conclusion

I am so close to find an actual solution to my bothering, but I spent too much of time solving it. I will surely do in the future if and only if I have more time for it. ;)