#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 12 15:51:09 2020

@author: aymeric
"""

"""
This python file is a placeholder of useful functions
for creating an iCal file. Now that I think about it
I could have used Class on which to instantiate methods. 
"""

## Global variables

months_dict = dict()
months_dict = {'janvier': '01','fevrier': '02', 'mars': '03', 'avril' :'04',
               'mai': '05', 'juin': '06', 'juillet': '07', 'aout': '08',
               'septembre': '09', 'octobre': '10','novembre': '11',
               'decembre': '12'}
months = [months_dict[0,:]]

#str.find() will be very useful!


## Function definitions

def HeaderOfiCal(name_file):
    """
    Creates header of ical file
    
    
    Parameters
    ----------
    name_file: string containing file name and extension
    
    Returns
    -------
    Nothing.
    """
    file = open(name_file,"w")
    header = "BEGIN:VCALENDAR\nVERSION:2.0\nPRODID:-//hacksw/handcal//NONSGML v1.0//EN\nMETHOD:PUBLISH\nX-WR-TIMEZONE:Europe/Paris"
    file.write(header)
    file.close()


def WriteEvent(name_file, day_date, time_slot, event_name):
    """
    Opens an iCal file, generates an event and writes in it
    
    
    Parameters
    ----------
    name_file: string containing name of file and its' extension

    day_date: list
        integer list of length 3
        [year, month, day]
              
    time_slot: list
        integer list of length 4
        [int beginning_hour, int beginning_minute, int ending_hour, 
        int ending_minute]
    
    event_name: list
        
    
    Returns
    -------
    Nothing.
    """
    file = open(name_file, "w")
    file.write("BEGIN:VEVENT")
    
    file.write("END:VEVENT")
    file.close()


def EventInfo(long_string):
    """
    From a long string returns dictionnaries of relevant information on event

    Parameters
    ----------
    long_string : str
        Contains portion to be analyzed

    Returns
    -------
    day_date : list
        integer list of length 3
        [year, month, day]
    """

