#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 16 18:54:16 2020

@author: Aymeric Wang
"""

"""---------Imports---------"""
from selenium import webdriver
import time
import config
import json
"""-------------------------"""

## Script

# Initializing web browser
browser = webdriver.Safari() # Chrome() or Firefox()
browser.get(config.university_webpage)
time.sleep(3) # waits for login page

# Looks the "username" box and writes in it
user = browser.find_element_by_id("username")
user.click()
user.send_keys(config.user_id_str)
time.sleep(2.5) # waits for machine to type in box

# Same for "password"
action = browser.find_element_by_id("password")
action.click()
action.send_keys(config.user_password_str)
time.sleep(2.5)

# Submission
Submission = browser.find_element_by_name("submit")
Submission.click()
time.sleep(10) # waits for web page to load

## From that point, this is specific to the college web page
Calendar_icon = browser.find_element_by_id("menu-option-31513")
time.sleep(4.75)
Calendar_icon.click()


Student_cal = browser.find_element_by_id("calendar_0")
time.sleep(3.75)
Student_cal.click()


Student_cal_events = browser.find_element_by_id("mOdAl_2_body")
time.sleep(4)
Student = Student_cal_events.get_attribute('innerHTML')
print("----------------------------\n")
print("This is former week\n\n")
print(Student,"\n\n")
time.sleep(2)

Cal_next_button = browser.find_element_by_id("calendar_next_button")
Cal_next_button.click()
time.sleep(0.5)

Student_cal_events = browser.find_element_by_id("mOdAl_2_body")
time.sleep(4)
Student = Student_cal_events.get_attribute('innerHTML')
print("----------------------------\n")
print("This is next week\n\n")
print(Student,"\n\n")
time.sleep(2)

Cal_next_button = browser.find_element_by_id("calendar_next_button")
Cal_next_button.click()
time.sleep(0.5)

Student_cal_events = browser.find_element_by_id("mOdAl_2_body")
time.sleep(4)
Student = Student_cal_events.get_attribute('innerHTML')
print("----------------------------\n")
print("This is two weeks from now\n\n")
print(Student,"\n\n")
time.sleep(2)

sc_text = open("web_page_sc.txt","w")
sc_text.truncate(0)

sc_text.close()

browser.quit()

