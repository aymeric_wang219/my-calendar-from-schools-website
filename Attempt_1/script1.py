#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 16 18:54:16 2020

@author: Aymeric Wang
"""

"""---------Imports---------"""
from selenium import webdriver
import time
import config
import requests
"""-------------------------"""

## Script

# Initializing web browser
browser = webdriver.Safari() # Chrome() or Firefox()
browser.get(config.university_webpage)

# Looks the "username" box and writes in it
user = browser.find_element_by_id("username")
user.click()
user.send_keys(config.user_id_str)
time.sleep(0.5) # waits for machine to type in box

# Same for "password"
action = browser.find_element_by_id("password")
action.click()
action.send_keys(config.user_password_str)
time.sleep(0.5)

# Submission
Submission = browser.find_element_by_name("submit")
Submission.click()
time.sleep(7.5) # waits for web page to load

## From that point, this is specific to the college web page
Calendar = browser.find_element_by_xpath('//*[@id="form:j_idt752"]')
Calendar.click()

A=browser.page_source
time.sleep(5)

# Not working past this line

Next = browser.find_element_by_xpath('//*[@id="form:j_idt117_container"]/div[1]/div[1]/button[4]')
time.sleep(4)
Next.click()
B=browser.page_source
time.sleep(5)

Event = browser.find_element_by_xpath('//*[@id="form:j_idt117_container"]/div[2]/div/table/tbody/tr/td/div/div/div[3]/table/tbody/tr/td[4]/div/div[2]/a[1]')
time.sleep(5)

Next = browser.find_element_by_xpath('//*[@id="form:j_idt117_container"]/div[1]/div[1]/button[4]/span')
time.sleep(4)
Next.click()
C=browser.page_source
time.sleep(20)

browser.quit()

sc_text = open("web_page_sc.txt","w+")
sc_text.truncate(0)
sc_text.write(C)

time.sleep(5)
sc_text.close()
