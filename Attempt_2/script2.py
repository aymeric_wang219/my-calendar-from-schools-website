#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 16 19:06:40 2020

@author: Aymeric Wang
"""

"""---------Remarks---------"""
"""To be interpreted without errors, script.py file 
has to be placed in the same dossier as .ics/.ical 
file to work. You can do that in the script with the 
os library."""
"""-------------------------"""

"""---------Imports---------"""
from selenium import webdriver
import time
import config
"""-------------------------"""

## Defining functions

def RelevantEvents(Criteria:list,lines:list):
    """Filter .ics file relevant events w. set of criteria.
    Looks at the lines of an .ics file stored as strings in
    a list. Then sees if an event meets criteria stored in 
    another list of strings. Returns a list."""

    new_lines = []
    new_lines+=lines[0:5]

    for line_nb in range(4,len(lines)):
        if 'BEGIN:VEVENT' in lines[line_nb]:
            event_begin = line_nb
            event_end = line_nb
            while not ('END:VEVENT' in lines[event_end]):
                event_end += 1
            
            relevant = False
            for line_value in lines[event_begin:event_end]:
                print(line_value)
                for criterion in config.Criteria:
                    if criterion in line_value :
                        relevant = True
                        break

            if relevant :
                    new_lines += lines[event_begin:event_end+1]

    return(new_lines)


## Script

# Initializing web browser
browser = webdriver.Safari() # Chrome() or Firefox()
browser.get(config.university_webpage)
time.sleep(2)
browser.quit()

# Opening .ics file
file = open(config.ics_filename,"r+")
lines = file.readlines()
file.close()

# Rewritting .ics file line by line
new_lines = RelevantEvents(config.Criteria,lines)

file = open(config.ics_filename,"r+")
file.truncate(0)
for line in new_lines:
    file.write(line)
file.close()