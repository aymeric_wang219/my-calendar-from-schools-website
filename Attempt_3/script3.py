#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Aug 22 13:50:54 2020

@author: Aymeric Wang
"""

import requests
import config
# from selenium import webdriver


session = requests.Session()
session.post(config.url,config.user_data)

calendar_page = session.get(config.url)
calendar_sc = calendar_page.text
calendar_sc = calendar_sc.split('\n')

sc_text = open("web_page_sc.txt","w+")
sc_text.truncate(0)

for row in calendar_sc:
    row = row + '\n'
    sc_text.write(row)

sc_text.close()